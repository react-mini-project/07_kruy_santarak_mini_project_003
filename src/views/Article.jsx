import React, { useEffect, useState } from "react";
import { Container, Row, Col, Button, Form } from "react-bootstrap";
import { useDispatch, useSelector } from "react-redux";
import { bindActionCreators } from "redux";
import { fetchArticleById, postArticle, updateArticleById, uploadImage } from "../redux/Action/ArticleAction";
import { fetchAllAuthors } from "../redux/Action/AuthorAction";
import Swal from 'sweetalert2'
import { useParams } from "react-router";
import { lang } from "../localization/localize";

export default function Article() {

    const [title, setTitle] = useState('')
    const [description, setDescription] = useState('')
    const [imageURL, setImageURL] = useState('https://www.bkgymswim.com.au/wp-content/uploads/2017/08/image_large.png')
    const [imageFile, setImageFile] = useState(null)
    const [author, setAuthor] = useState([])
    const [authorId, setAuthorId] = useState(null)

    const dispatch = useDispatch();
    const state = useSelector((state) => state.AuthorReducer.authors);
    const onPost = bindActionCreators(postArticle, dispatch);
    const onUploadImg = bindActionCreators(uploadImage, dispatch);
    const onUpdateArticle = bindActionCreators(updateArticleById, dispatch);

    const {id} = useParams();
  
    useEffect(() => {
      if(id){
        dispatch(fetchArticleById(id)).then(article=>{
          setTitle(article.payload.title)
          setDescription(article.payload.description)
          setImageURL(article.payload.image)
        });
      }
      dispatch(fetchAllAuthors()).then(author=>{
        setAuthorId(author[0])
        setAuthor(author)
      });
    }, []);


    const onAdd = async(e)=>{
        e.preventDefault()
        let article = {
            title,description,author: authorId
        }
        
        if(imageFile){
            let url = await onUploadImg(imageFile)
            article.image = url.payload
         }
        onPost(article).then(message=>{
          Swal.fire({
            position: 'top',
            icon: 'success',
            title: message.payload,
            showConfirmButton: false,
            timer: 1500
          })
        })
        setTitle('')
        setDescription('')
    }

    const onUpdate = async(e)=>{
      e.preventDefault()
      let article = {
          title,description,author: authorId
      }
      
      if(imageFile){
          let url = await onUploadImg(imageFile)
          article.image = url.payload
       }
      onUpdateArticle(id,article).then(message=>{
        Swal.fire({
          position: 'top',
          icon: 'success',
          title: message.payload,
          showConfirmButton: false,
          timer: 1500
        })
      })
      setTitle('')
      setDescription('')
  }

  return (
    <Container className="my-4">
      <h1 className="my-2">{id ? lang.UpdateArticle : lang.AddArticle}</h1>
      <Row>
        <Col md={8}>
          <Form>
            <Form.Group controlId="title">
              <Form.Label>{lang.Title}</Form.Label>
              <Form.Control
                type="text"
                placeholder="Title"
                value={title}
                onChange={(e) => setTitle(e.target.value)}
              />
              <Form.Text className="text-muted"></Form.Text>
            </Form.Group>

            <Form.Group controlId="description">
              <Form.Label>{lang.Author}</Form.Label>
              <Form.Control
                as="select"
                aria-label="Choose Author"
                onChange={(e)=>setAuthorId(e.target.value)}
              >
                {state.map((author) => (
                  <option
                    key={author._id}
                    value={author._id}
                    selected={author._id === authorId}
                  >
                    {author.name}
                  </option>
                ))}
              </Form.Control>
            </Form.Group>

            <Form.Group controlId="description">
              <Form.Label>{lang.Description}</Form.Label>
              <Form.Control
                as="textarea"
                rows={4}
                placeholder="Description"
                value={description}
                onChange={(e) => setDescription(e.target.value)}
              />
            </Form.Group>
            <Button
              variant="primary"
              type="submit"
              disabled={
                title === '' && description === '' || 
                title !== '' && description === '' ||
                title === '' && description !== ''
              }
              onClick={id ? onUpdate : onAdd}
            >
              {id ? lang.Save : lang.Submit}
            </Button>
          </Form>
        </Col>
        <Col md={4}>
          <img className="w-100" src={imageURL} alt="default pic"/>
          <Form>
            <Form.Group>
              <Form.File
                id="img"
                label={lang.ChooseImage}
                onChange={(e) => {
                  let url = URL.createObjectURL(e.target.files[0]);
                  setImageFile(e.target.files[0]);
                  setImageURL(url);
                }}
              />
            </Form.Group>
          </Form>
        </Col>
      </Row>
    </Container>
  );
}
