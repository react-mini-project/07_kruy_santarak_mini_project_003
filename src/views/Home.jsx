import React, { useEffect, useState } from "react";
import { useDispatch, useSelector, useStore } from "react-redux";
import { deleteArticle, fetchAllArticles } from "../redux/Action/ArticleAction";
import { Container, Row, Col, Card, Button } from "react-bootstrap";
import { bindActionCreators } from "redux";
import { useHistory } from "react-router";
import { fetchAllCategories } from "../redux/Action/CategoryAction";
import ReactPaginate from "react-paginate";
import { lang } from "../localization/localize";
import { GoogleLogin } from "react-google-login";

export default function Home() {
  const [name, setName] = useState("");
  const [profile, setProfile] = useState("");

  const dispatch = useDispatch();
  const onDelete = bindActionCreators(deleteArticle, dispatch);
  const article = useSelector((state) => state.ArticleReducer);
  const category = useSelector((state) => state.CategoryReducer.categories);

  const history = useHistory();
  const [page, setPage] = useState(1);

  useEffect(() => {
    dispatch(fetchAllArticles(page));
    dispatch(fetchAllCategories());
  }, []);

  const onPageChange = ({ selected }) => {
    dispatch(fetchAllArticles(selected + 1));
  };

  const responseGoogle = (response) => {
    setName(response.profileObj.name);
    setProfile(response.profileObj.imageUrl);
  };
  return (
    <Container className="my-4">
        <Row>
          <Col md="3">
            <Card>
              <Card.Img
                variant="top"
                style={{ objectFit: "cover", height: "250px" }}
                src={
                  profile !== ""
                    ? profile
                    : "https://variety.com/wp-content/uploads/2016/05/pikachu.jpg?w=674"
                }
              />
              <Card.Body>
                <Card.Title>
                  {name === "" ? "Sign in with Google" : name}
                </Card.Title>
                {name === "" ? (
                  <GoogleLogin
                    clientId="574972905830-qd51f8ak2i4k4eo7i4suuvqsvi2in442.apps.googleusercontent.com"
                    buttonText="Login"
                    onSuccess={responseGoogle}
                    onFailure={responseGoogle}
                    cookiePolicy={"single_host_origin"}
                  />
                ) : (
                  <Button
                    variant="danger"
                    onClick={() => {
                      setName("");
                      setProfile("");
                    }}
                  >
                    Logout
                  </Button>
                )}
              </Card.Body>
            </Card>
          </Col>
          <Col md="9">
            <h1>{lang.Category}</h1>
            <Row className="my-3">
              {category.map((item, index) => (
                <Button
                  key={index}
                  className="mx-1"
                  variant="outline-secondary"
                  size="sm"
                >
                  {item.name}
                </Button>
              ))}
            </Row>
            <Row className="my-3">
              {article.articles.map((item, index) => (
                <Col key={index} md="4">
                  <Card className="mb-4">
                    <Card.Img
                      variant="top"
                      style={{ objectFit: "cover", height: "150px" }}
                      src={item.image}
                    />
                    <Card.Body>
                      <Card.Title>{item.title}</Card.Title>
                      <Card.Text className="text-line-3">
                        {item.description}
                      </Card.Text>
                      <Button
                        variant="primary"
                        size="sm"
                        onClick={() => history.push(`/view/${item._id}`)}
                      >
                        {lang.View}
                      </Button>{" "}
                      <Button
                        variant="warning"
                        size="sm"
                        onClick={() => {
                          history.push(`/update/article/${item._id}`);
                        }}
                      >
                        {lang.Update}
                      </Button>{" "}
                      <Button
                        variant="danger"
                        size="sm"
                        onClick={() => onDelete(item._id)}
                      >
                        {lang.Delete}
                      </Button>
                    </Card.Body>
                  </Card>
                </Col>
              ))}
            </Row>
            <ReactPaginate
              pageCount={article.totalPage}
              onPageChange={onPageChange}
              containerClassName="pagination pagination-sm justify-content-center"
              pageClassName="page-item btn btn-outline-primary btn-sm"
              pageLinkClassName="page-link"
              previousClassName="page-item btn btn-outline-primary btn-sm"
              previousLinkClassName="page-link"
              nextLinkClassName="page-link"
              nextClassName="page-item btn btn-outline-primary btn-sm"
              activeClassName="active"
            />
          </Col>
        </Row>
    </Container>
  );
}
