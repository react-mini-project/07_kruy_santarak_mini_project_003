import React, { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { deleteAuthor, fetchAllAuthors, postAuthor, updateAuthor } from "../redux/Action/AuthorAction";
import { Container, Form, Row, Col, Button, Table } from "react-bootstrap";
import { bindActionCreators } from "redux";
import { uploadImage } from "../redux/Action/ArticleAction";
import Swal from 'sweetalert2'
import { lang } from "../localization/localize";


export default function Author() {
  const [imageURL, setImageURL] = useState(
    "https://www.bkgymswim.com.au/wp-content/uploads/2017/08/image_large.png"
  );
  const [imageFile, setImageFile] = useState(null);
  const [name, setName] = useState('');
  const [email, setEmail] = useState('');
  const [isUpdate, setIsUpdate] = useState(null);
  const [isRefresh, setIsRefresh] = useState(false);

  const dispatch = useDispatch();
  const onDelete = bindActionCreators(deleteAuthor, dispatch);
  const onUploadImg = bindActionCreators(uploadImage, dispatch);
  const onPost = bindActionCreators(postAuthor, dispatch);
  const onUpdateAuthor= bindActionCreators(updateAuthor, dispatch);
  const state = useSelector((state) => state.AuthorReducer.authors);

  useEffect(() => {
    dispatch(fetchAllAuthors());
    setIsRefresh(false)
  }, [isRefresh]);

  const onAdd = async (e) => {
    e.preventDefault();
    let newAuthor = { name, email };

    if (imageFile) {
      let url = await onUploadImg(imageFile);
      newAuthor.image = url.payload;
    }
    onPost(newAuthor).then((message) => {
        setIsRefresh(true)
        Swal.fire({
            position: 'top',
            icon: 'success',
            title: message.payload,
            showConfirmButton: false,
            timer: 1500
          })
    });
    setImageFile(null);
    setName('');
    setEmail('');
    clear();
  };

  function clear(){
    document.getElementById("name").value = "";
    document.getElementById("email").value = "";
    document.getElementById("img").value = "";
    document.getElementById("image").src =
      "https://www.bkgymswim.com.au/wp-content/uploads/2017/08/image_large.png";
  }

  const onEdit = (id, name, email, image) => {
    setIsUpdate(id)
    setName(name)
    setEmail(email)
    document.getElementById("name").value = name;
    document.getElementById("email").value = email;
    document.getElementById("image").src = image;
  };

  const onUpdate = async (e) => {
    e.preventDefault();
    let newAuthor = { name, email };

    if (imageFile) {
        let url = await onUploadImg(imageFile);
        newAuthor.image = url.payload;
      }

    onUpdateAuthor(isUpdate, newAuthor).then((message) => {
        Swal.fire({
            position: 'top',
            icon: 'success',
            title: message.payload,
            showConfirmButton: false,
            timer: 1500
          })
    });
    setIsRefresh(true)
    setIsUpdate(null);
    setImageFile(null);
    setName('');
    setEmail('');
    clear();
  };

  return (
    <div>
      <Container>
        <h1>{lang.Author}</h1>
        <Row>
          <Col md={8}>
            <Form>
              <Form.Group>
                <Form.Label>{lang.Author}</Form.Label>
                <Form.Control
                  id="name"
                  type="text"
                  placeholder="Author Name"
                  onChange={(e) => setName(e.target.value)}
                />
                <Form.Text className="text-muted"></Form.Text>
              </Form.Group>

              <Form.Group>
                <Form.Label>{lang.Email}</Form.Label>
                <Form.Control
                  id="email"
                  type="text"
                  placeholder="Email"
                  onChange={(e) => setEmail(e.target.value)}
                ></Form.Control>
              </Form.Group>
              <Button
                variant="primary"
                type="submit"
                disabled={
                  name === '' && email === '' || 
                  name !== '' && email === '' ||
                  name === '' && email !== ''
                }
                onClick={isUpdate !== null ? onUpdate : onAdd}
              >
                {isUpdate !== null ? lang.Save : lang.Add}
              </Button>
            </Form>
          </Col>
          <Col md={4}>
            <img id="image" className="w-75" alt="..." src={imageURL} />
            <Form>
              <Form.Group>
                <Form.File 
                  className="mt-2"
                  id="img"
                  onChange={(e) => {
                    let url = URL.createObjectURL(e.target.files[0]);
                    setImageFile(e.target.files[0]);
                    setImageURL(url);
                  }}
                />
              </Form.Group>
            </Form>
          </Col>
        </Row>

        <Table striped bordered hover>
          <thead>
            <tr>
              <th>#</th>
              <th>{lang.Name}</th>
              <th>{lang.Email}</th>
              <th>{lang.Img}</th>
              <th>{lang.Action}</th>
            </tr>
          </thead>
          <tbody>
            {state.map((item, index) => (
              <tr key={index}>
                <td>{item._id}</td>
                <td>{item.name}</td>
                <td>{item.email}</td>
                <td>
                  <img src={item.image} alt="" width="auto" height={100} />
                </td>
                <td>
                  <Button
                    size="sm"
                    variant="warning"
                    onClick={() =>
                      onEdit(item._id, item.name, item.email, item.image)
                    }
                  >
                    {lang.Update}
                  </Button>{" "}
                  <Button
                    size="sm"
                    variant="danger"
                    onClick={() => onDelete(item._id)}
                  >
                    {lang.Delete}
                  </Button>
                </td>
              </tr>
            ))}
          </tbody>
        </Table>
      </Container>
    </div>
  );
}
