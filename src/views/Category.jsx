import React, { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { deleteCategory, fetchAllCategories, postCategory, updateCategory } from "../redux/Action/CategoryAction";
import { Container, Form, Table, Button } from "react-bootstrap";
import { lang } from "../localization/localize";
import { bindActionCreators } from "redux";
import Swal from 'sweetalert2'

export default function Category() {
  const [isUpdate, setIsUpdate] = useState(null);
  const [newCategory, setNewCategory] = useState(null);
  const [isRefresh, setIsRefresh] = useState(false);

  const dispatch = useDispatch();
  const state = useSelector((state) => state.CategoryReducer.categories);
  const onDelete = bindActionCreators(deleteCategory, dispatch);
  const onPostCategory = bindActionCreators(postCategory, dispatch);
  const onUpdateeCategory = bindActionCreators(updateCategory, dispatch);

  useEffect(() => {
    dispatch(fetchAllCategories());
    setIsRefresh(false)
  }, [isRefresh]);

  const onAdd = async (e) => {
    e.preventDefault();
    let category1 = { name: newCategory };
    onPostCategory(category1).then(message => {
      setIsRefresh(true)
      Swal.fire({
        position: 'top',
        icon: 'success',
        title: message.payload,
        showConfirmButton: false,
        timer: 1500
      })
    });
    document.getElementById("text").value = "";
  };

  const onUpdate = (id, name) => {
    setIsUpdate(id);
    setNewCategory(name);
    document.getElementById("text").value = name;
  };

  const onUpdatee = async (e) => {
    e.preventDefault();
    let category1 = { name: newCategory };
    onUpdateeCategory(isUpdate, category1).then(message => {
      setIsRefresh(true)
      Swal.fire({
        position: 'top',
        icon: 'success',
        title: message.payload,
        showConfirmButton: false,
        timer: 1500
      })
    });
    setIsUpdate(null);
    document.getElementById("text").value = "";
  };

  return (
    <div>
      <Container>
        <h1 className="my-3">{lang.Category}</h1>
        <Form>
          <Form.Group>
            <Form.Label>{lang.Category}</Form.Label>
            <Form.Control
              id="text"
              type="text"
              placeholder="Category Name"
              onChange={(e) => setNewCategory(e.target.value)}
            />
            <br />
            <Button
              type="submit"
              variant="secondary"
                onClick={isUpdate !== null ? onUpdatee : onAdd}
            >
              {isUpdate !== null ? lang.Save : lang.Add}
            </Button>
            <Form.Text className="text-muted"></Form.Text>
          </Form.Group>
        </Form>
        <Table striped bordered hover>
          <thead>
            <tr>
              <th>ID</th>
              <th>{lang.Name}</th>
              <th>{lang.Action}</th>
            </tr>
          </thead>
          <tbody>
            {state.map((item, index) => (
              <tr key={index}>
                <td>{index + 1}</td>
                <td>{item.name}</td>
                <td>
                  <Button
                    size="sm"
                    variant="warning"
                      onClick={()=>onUpdate(item._id, item.name)}
                  >
                    {lang.Update}
                  </Button>{" "}
                  <Button
                    size="sm"
                    variant="danger"
                    onClick={() => onDelete(item._id)}
                  >
                    {lang.Delete}
                  </Button>
                </td>
              </tr>
            ))}
          </tbody>
        </Table>
      </Container>
    </div>
  );
}
