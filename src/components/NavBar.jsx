import React, { useContext, useEffect } from "react";
import { Navbar, NavDropdown, Nav, Form, Button, FormControl, Container } from "react-bootstrap";
import { NavLink } from "react-router-dom";
import { lang } from "../localization/localize";
import { LangContext } from "../util/LangContext";

export default function MyNavBar() {

  const context = useContext(LangContext)

    useEffect(() => {
      if(localStorage.getItem('language') !== null){
        context.setLang(localStorage.getItem('language'))
        lang.setLanguage(localStorage.getItem('language'))
      }else{
        localStorage.setItem('language', 'km');
        context.setLang(localStorage.getItem('language'))
        lang.setLanguage(localStorage.getItem('language'))
      }
  }, [])

  const onChangeLanguage = (language) =>{
    context.setLang(language);
    lang.setLanguage(language);
    localStorage.setItem('language', language);
  }

  return (
    <div>
      <Navbar bg="success" className="navbar navbar-dark" expand="lg">
          <Container>
        <Navbar.Brand href="#">Redux</Navbar.Brand>
        <Navbar.Toggle aria-controls="navbarScroll" />
        <Navbar.Collapse id="navbarScroll">
          <Nav
            className="mr-auto my-2 my-lg-0"
            style={{ maxHeight: "100px" }}
            navbarScroll
          >
            <Nav.Link as={NavLink} to='/'>{lang.Home}</Nav.Link>
            <Nav.Link as={NavLink} to='/article'>{lang.Article}</Nav.Link>
            <Nav.Link as={NavLink} to='/author'>{lang.Author}</Nav.Link>
            <Nav.Link as={NavLink} to='/category'>{lang.Category}</Nav.Link>
            <NavDropdown title={lang.Language} id="navbarScrollingDropdown">
              <NavDropdown.Item onClick={()=>onChangeLanguage('km')}>{lang.Khmer}</NavDropdown.Item>
              <NavDropdown.Item onClick={()=>onChangeLanguage('en')}>
                English
              </NavDropdown.Item>
            </NavDropdown>
          </Nav>
          <Form className="d-flex">
            <FormControl
              type="search"
              placeholder="Search"
              className="mr-2"
              aria-label="Search"
            />
            <Button variant="outline-light">Search</Button>
          </Form>
        </Navbar.Collapse>
        </Container>
      </Navbar>
    </div>
  );
}
