import API from "../../util/API";

export const fetchAllArticles = (page) => async dispatch =>{

    let response = await API.get(`/articles?page=${page}&size=6`);

    return dispatch({
        type: "FETCH_ARTICLE",
        payload: response.data
    })
}

export const deleteArticle = (id) => async dispatch =>{

    let response = await API.delete(`/articles/${id}`);

    return dispatch({
        type: "DELETE_ARTICLE",
        payload: response.data.data
    })
}

export const postArticle = (article) => async dispatch => {
    let response = await API.post('/articles', article)

    return dispatch({
        type: "POST_ARTICLE",
        payload: response.data.message
    })
}

export const uploadImage = (file) => async dispatch => {

    let formData = new FormData()
    formData.append('image', file)

    let response = await API.post('/images', formData)

    return dispatch({
        type: "UPLOAD_IMG",
        payload: response.data.url
    })
}

export const fetchArticleById = (id) => async dispatch => {
    let response = await API.get(`articles/${id}`)
    
    return dispatch({
        type: "FETCH_ARTICLE_BY_ID",
        payload: response.data.data
    })
}

export const updateArticleById = (id, article) => async dispatch => {
    let response = await API.patch(`/articles/${id}`, article)
    
    return dispatch({
        type: "UPDATE_ARTICLE_BY_ID",
        payload: response.data.message
    })
}