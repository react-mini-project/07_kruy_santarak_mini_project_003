import API from "../../util/API"

export const fetchAllAuthors = () => async dispatch =>{

    let response = await API.get('/author')

    return dispatch({
        type: "FETCH_AUTHOR",
        payload: response.data.data
    })
}

export const postAuthor = (author) => async dispatch =>{

    let response = await API.post('/author', author)

    return dispatch({
        type: "POST_AUTHOR",
        payload: response.data.message
    })
}

export const deleteAuthor = (id) => async dispatch => {

    let response = await API.delete(`/author/${id}` )

    return dispatch({
        type: "DELETE_AUTHOR",
        payload: response.data.data
    })
}

export const updateAuthor = (id, author) => async dispatch => {

    let response = await API.put(`/author/${id}`, author )

    return dispatch({
        type: "UPDATE_AUTHOR",
        payload: response.data.message
    })
}