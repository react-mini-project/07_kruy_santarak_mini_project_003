import API from "../../util/API"

export const fetchAllCategories = () => async dispatch => {

    let response = await API.get('/category')

    return dispatch({
        type: "FETCH_CATEGORY",
        payload: response.data.data
    })
}

export const postCategory = (category) => async dispatch => {

    let response = await API.post('/category', category)

    return dispatch({
        type: "POST_CATEGORY",
        payload: response.data.message
    })
}

export const deleteCategory = (id) => async dispatch => {

    let response = await API.delete(`/category/${id}`)

    return dispatch({
        type: "DELETE_CATEGORY",
        payload: response.data.data
    })
}

export const updateCategory = (id, category) => async dispatch => {

    let response = await API.put(`/category/${id}`, category)

    return dispatch({
        type: "UPDATE_CATEGORY",
        payload: response.data.message
    })
}