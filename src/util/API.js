import axios from "axios";

const API = axios.create({
    baseURL: 'http://110.74.194.124:3034/api',
});

export default API;